import "./App.css";
import React from "react";
import CONSTANTS from "./constants";
import MainSection from "./components/main-section";
import SearchField from "./components/search-field";
import { weatherAPI } from "./axios-config";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      latitude: "",
      longitude: "",
      weatherData: "",
      city: "",
      error: "",
    };
    this.locateUser = this.locateUser.bind(this);
    this.getCoordinates = this.getCoordinates.bind(this);
    this.setCoordinates = this.setCoordinates.bind(this);
    this.fetchWeatherData = this.fetchWeatherData.bind(this);
    this.setWeatherData = this.setWeatherData.bind(this);
    this.renderIfNoErrors = this.renderIfNoErrors.bind(this);
    this.setErrorMessage = this.setErrorMessage.bind(this);
  }

  getCoordinates() {
    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(resolve, reject, {
        maximumAge: 600000,
        timeout: 20000,
        enableHighAccuracy: true,
      });
    });
  }

  setCoordinates(location) {
    this.setState(
      {
        latitude: location.coords.latitude,
        longitude: location.coords.longitude,
        error: "",
      },
      this.getWeatherData
    );
  }

  setErrorMessage(error) {
    this.setState({
      error: error.message,
    });
  }
  fetchWeatherData(latitude, longitude) {
    this.setState(
      {
        weatherData: "",
      },
      () => {
        weatherAPI
          .get("", {
            params: {
              lat: latitude,
              lon: longitude,
            },
          })
          .then((response) => {
            this.setWeatherData(response.data);
          })
          .catch((error) => {
            this.setErrorMessage(error);
          });
      }
    );
  }

  setWeatherData(weatherData) {
    this.setState({
      weatherData: weatherData,
    });
  }

  getWeatherData() {
    this.fetchWeatherData(this.state.latitude, this.state.longitude);
  }

  locateUser() {
    this.setState(
      {
        weatherData: "",
      },
      () => {
        this.getCoordinates()
          .then((response) => {
            this.setCoordinates(response);
          })
          .catch((error) => {
            this.setErrorMessage(error);
          });
      }
    );
  }

  componentDidMount() {
    this.locateUser();
  }

  renderIfNoErrors() {
    if (this.state.error) {
      if (this.state.error.toLowerCase().includes("geolocation")) {
        return <h1>Please enable location or search for location manually.</h1>;
      }
      return <h1>{JSON.stringify(this.state.error)}</h1>;
    } else {
      return <MainSection weatherData={this.state.weatherData} />;
    }
  }
  render() {
    return (
      <div className="App">
        <header className="header">
          <p>{CONSTANTS.APP_NAME}</p>
          <SearchField onChange={this.setCoordinates} />
          <button onClick={this.locateUser}>Locate Me</button>
        </header>
        {this.renderIfNoErrors()}
      </div>
    );
  }
}

export default App;
