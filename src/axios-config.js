import axios from "axios";
import CONSTANTS from "./constants";

const weatherAPI = axios.create({
  baseURL: CONSTANTS.API_ENDPOINTS.OPEN_WEATHER.WEATHER,
  params: {
    appid: process.env.REACT_APP_OPEN_WEATHER_API_KEY,
    units: "metric",
  },
});

const geolocationAPI = axios.create({
  baseURL: CONSTANTS.API_ENDPOINTS.OPEN_WEATHER.GEOLOCATION,
  params: {
    appid: process.env.REACT_APP_OPEN_WEATHER_API_KEY,
    limit: 5,
  },
});

export { weatherAPI, geolocationAPI };
