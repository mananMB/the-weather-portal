import React from "react";

class Card extends React.Component {
  render() {
    return (
      <div className="card">
        <div className="card-label">{this.props.label}</div>
        <div className="card-content">{this.props.content}</div>
      </div>
    );
  }
}

export default Card;
