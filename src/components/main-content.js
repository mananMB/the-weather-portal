import React from "react";
import Card from "./card";

class MainContent extends React.Component {
  render() {
    function padTo2Digits(num) {
      return num.toString().padStart(2, "0");
    }

    function getHHMM(unixTime) {
      const date = new Date(unixTime * 1000);
      const hours = date.getHours();
      const minutes = date.getMinutes();
      return `${padTo2Digits(hours)}:${padTo2Digits(minutes)}`;
    }

    return (
      <div className="container">
        <div className="heading">Current Highlights</div>
        <div className="row">
          <Card
            label="Feels Like"
            content={`${Math.floor(
              Number(this.props.weatherData.main.feels_like)
            )}°C`}
          />
          <Card
            label="Humidity"
            content={`${this.props.weatherData.main.humidity}%`}
          />
          <Card
            label="Pressure"
            content={`${this.props.weatherData.main.pressure} hPa`}
          />
          <Card
            label="Visibility"
            content={`${Math.floor(
              Number(this.props.weatherData.visibility) / 1000
            )} km`}
          />
          <Card
            label="Wind"
            content={`${this.props.weatherData.wind.speed} m/s`}
          />
          <Card
            label="Sunset"
            content={getHHMM(this.props.weatherData.sys.sunset)}
          />
        </div>
      </div>
    );
  }
}

export default MainContent;
