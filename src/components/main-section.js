import React from "react";
import CONSTANTS from "../constants";
import Sidebar from "./sidebar";
import MainContent from "./main-content";
import Loader from "./loader";

class MainSection extends React.Component {
  render() {
    const mainSectionStyles = {
      display: "flex",
      background: CONSTANTS.COLORS.BLACK,
      width: "100%",
      boxSizing: "border-box",
      color: CONSTANTS.COLORS.WHITE,
    };
    if (this.props.weatherData === "") {
      return (
        <section style={mainSectionStyles} className="main-section">
          <Loader />
        </section>
      );
    } else {
      return (
        <section style={mainSectionStyles} className="main-section">
          <Sidebar weatherData={this.props.weatherData} />
          <MainContent weatherData={this.props.weatherData} />
          {/*{JSON.stringify(this.props.weatherData)}*/}
        </section>
      );
    }
  }
}

export default MainSection;
