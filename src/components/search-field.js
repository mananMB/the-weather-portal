import React from "react";
import CONSTANTS from "../constants";
import { geolocationAPI } from "../axios-config";

class SearchField extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      searchData: "",
      searchFieldValue: "",
      city: "",
    };
    this.handleSearchFieldChange = this.handleSearchFieldChange.bind(this);
    this.getSearchQuery = this.getSearchQuery.bind(this);
    this.getLocationCoordinates = this.getLocationCoordinates.bind(this);
  }

  getSearchQuery() {
    this.setState((state) => {
      return {
        city: state.searchFieldValue,
      };
    }, this.getLocationCoordinates);
  }

  handleSearchFieldChange(event) {
    this.setState(
      {
        searchFieldValue: event.target.value.trim(),
      },
      this.getSearchQuery
    );
  }

  fetchCitiesData(query) {
    geolocationAPI
      .get("", {
        params: {
          q: query,
        },
      })
      .then((response) => {
        this.setState({
          searchData: response.data,
        });
      });
  }

  getLocationCoordinates() {
    if (this.state.city.length <= 3) {
      this.setState(
        {
          searchData: "",
        },
        () => {
          if (this.state.city.length > 3 && this.state.city) {
            this.fetchCitiesData(this.state.city);
          }
        }
      );
    }
  }

  render() {
    const searchResultContainer = () => {
      if (this.state.city.length > 3 && this.state.searchData.length === 0) {
        return (
          <div className="search-result-container">
            <div className="search-value">No city found</div>{" "}
          </div>
        );
      } else if (this.state.searchData.length > 0) {
        return (
          <div className="search-result-container">
            {[...this.state.searchData].map((city, index) => {
              return (
                <div
                  key={index}
                  className="search-value"
                  onClick={() => {
                    this.props.onChange({
                      coords: { latitude: city.lat, longitude: city.lon },
                    });
                    this.setState({
                      searchData: "",
                    });
                  }}
                >
                  {city.name},{city.country}
                </div>
              );
            })}
          </div>
        );
      } else {
        return <div></div>;
      }
    };

    return (
      <div className="search-container">
        <input
          type="text"
          className="search-field"
          placeholder="Enter City Name"
          value={this.state.searchFieldValue}
          onChange={this.handleSearchFieldChange}
        />
        {searchResultContainer()}
      </div>
    );
  }
}

export default SearchField;
