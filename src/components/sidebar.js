import React from "react";
import moment from "moment";

class Sidebar extends React.Component {
  render() {
    return (
      <div
        className="sidebar"
        style={{
          display: "flex",
          flexDirection: "column",
          flex: "0 0 20%",
          padding: "2rem",
        }}
      >
        <div className="weather-overview">
          <div className="weather-icon">
            <img
              style={{ width: "100%", height: "auto" }}
              src={`http://openweathermap.org/img/wn/${this.props.weatherData.weather[0].icon}@2x.png`}
              alt={"weather-icon"}
            />
          </div>
        </div>
        <div style={{ fontSize: "4rem" }} className="temperature">
          {Math.floor(Number(this.props.weatherData.main.temp))}°C
        </div>
        <div style={{ paddingTop: "3rem", fontSize: "2rem" }} className="time">
          {moment().format("dddd, hh:mm")}
        </div>
        <div
          style={{
            paddingTop: "3rem",
            fontSize: "1.5rem",
            textTransform: "capitalize",
          }}
          className="weather-conditions"
        >
          {this.props.weatherData.weather[0].description}
        </div>
        <div
          style={{ paddingTop: "3rem", fontSize: "3rem" }}
          className="location-details"
        >
          {this.props.weatherData.name}
        </div>
      </div>
    );
  }
}

export default Sidebar;
