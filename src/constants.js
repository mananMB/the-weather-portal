const CONSTANTS = {
  APP_NAME: "The Weather Portal",
  COLORS: {
    BLACK: "#000000",
    WHITE: "#ffffff",
  },
  API_ENDPOINTS: {
    OPEN_WEATHER: {
      WEATHER: "https://api.openweathermap.org/data/2.5/weather",
      FORECAST: "https://api.openweathermap.org/data/2.5/forecast/",
      GEOLOCATION: "https://api.openweathermap.org/geo/1.0/direct",
    },
  },
};

export default CONSTANTS;
